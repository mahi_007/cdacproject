<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 
 <%@ taglib uri="http://www.springframework.org/tags/form" prefix="spr" %>   
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>

 <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" 
    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>
<body>
	 <div class="container-fluid">
        <div class="row">
            <div class="col-3"></div>
            <div class="col-6 mt-3" style="border: black 5px solid;background: rgb(186, 190, 192)" >
           
            <spr:form action="reg.htm" commandName="user">
                <div>
                    <h1 style="margin-left: 230px ">SignUp Form</h1>
                </div>

                <div class="form-group row">
                    <label for="inputName" class="col-sm-2 col-form-label">name</label>
                    <div class="col-sm-10">
                    <spr:input type="text" path="userName" class="form-control" id="inputPassword" placeholder="Enter your full Name"/>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="inputName" class="col-sm-2 col-form-label">age</label>
                    <div class="col-sm-10">
                    <spr:input type="text" path="age"  class="form-control" id="inputPassword" placeholder="Enter your age"/>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="inputPassword" class="col-sm-2 col-form-label">Phone no</label>
                    <div class="col-sm-10">
                    <spr:input type="text" path="phoneNo"   class="form-control" id="inputPassword" placeholder="Enter your 10 digit number"/>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="staticEmail" class="col-sm-2 col-form-label">Email</label>
                    <div class="col-sm-10">
                    <spr:input type="text"  path="emailId"    class="form-control" id="staticEmail" placeholder="Enter your email id"/>
                    </div>
                </div>
           
            
                <div class="form-group row">
                    <label for="inputPassword" class="col-sm-2 col-form-label">Password</label>
                    <div class="col-sm-10">
                    <spr:input type="password"  path="userPass"  class="form-control" id="inputPassword" placeholder="enter your password"/>
                    </div>
                </div>
               
                <div class="form-group row">
                    <label for="inputPassword" class="col-sm-2 col-form-label">Conform Password</label>
                    <div class="col-sm-10">
                    <spr:input type="password" path="userConformPass"   class="form-control" id="inputPassword" placeholder="enter your password"/>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="inputPassword" class="col-sm-2 col-form-label">Profession</label>
                        <div class="col-sm-10">
                            <spr:select  path="userRole" style="width: 150px">
                                <spr:option value="Vendor">Vendor</spr:option>
                                <spr:option value="User">User</spr:option>
                            </spr:select>
                        </div>
                </div>
                <div class="form-group row">
                    <label for="inputPassword" class="col-sm-2 col-form-label">About</label>
                        <div class="col-sm-10">                        
                            <spr:textarea  path="about"  class="form-control" rows="2" placeholder="enter about"></spr:textarea>
                        </div>
                </div>
                <div class="form-group row">
                    <label for="inputPassword" class="col-sm-2 col-form-label">Address</label>
                        <div class="col-sm-10">                        
                            <spr:textarea  path="address" class="form-control" rows="2" placeholder="enter address"></spr:textarea>
                        </div>
                </div>

					<input type="submit" value="submit" class="btn btn-success" style="margin-left: 170px"/>
               

            </spr:form>
            </div>
            <div class="col-3"></div>
        </div>
    </div>










</body>
</html>

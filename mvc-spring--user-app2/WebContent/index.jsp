<%@page import="com.cdac.springwebapp.dto.User"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 
 <%@ taglib uri="http://www.springframework.org/tags/form" prefix="spr" %>   
    
     <%
 	User user  = (User)session.getAttribute("user");
 
 %>   
    
<!DOCTYPE html>
<html>
<head>


<meta charset="ISO-8859-1">
<title>Insert title here</title>
 <style>
    /*.dropdown-submenu {
          position: relative;
        }
        
        .dropdown-submenu .dropdown-menu {
          top: 0;
          left: 100%;
          margin-top: -1px;
        } */

        #overlay ,#overlay1,#overlay2,#overlay3,#overlay4{
  position: fixed;
  display: none;
  width: 100%;
  height: 100%;
  position: fixed; 
  overflow-y: scroll;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: rgba(0,0,0,0.5);
  z-index: 2;
  cursor: pointer;
}

    body {
      font-family: 'Roboto Mono', monospace;
      background-color: #adadad;
    }

    .navbar {
      height: 46px;
      background-color: #1b1b1b;
    }

    .navbar a {
      float: left;
      font-size: 16px;
      color: white;
      text-align: center;
      padding: 14px 16px;
      text-decoration: none;
    }



    /*.container {
  max-width: 800px;
  height: 800px;
  background-color: white;
  margin: 10px auto 0 auto;
}*/
  </style>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" 
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<div class="container-fluid">

    <!-- TESTING-->


    <!-- HEADER -->
    <div class="row bg-dark" style="height:70px">
      <div class="col-4 d-flex justify-content-center align-items-center" style="font-size: 2rem;font-size: bold; font-family: cursive;color:white">
        EASE HOME
      </div>
      <div class="col-8">

        <div class="row" style="height:60px">
          <div class="col-2 d-flex justify-content align-items-center">
              <!--<select class="btn bg-light">
                  <option class="form-control" value="user">User</option>
                  <option class="form-control" value="vendor">Vendor</option>
              </select>-->
          </div>
          <div class="col-2 d-flex justify-content align-items-center">
            <a href="e_homepg.html" style="color:white">My Bookings</a>
          </div>
          <div class="col-2 d-flex justify-content align-items-center">
            <a  href="e_homepg.html" style="color:white">My Bookings</a>
          </div>
          <div class="col-2 d-flex justify-content align-items-center">
            <!--<a class="text-muted" href="e_homepg.html">My Bookings</a>-->
           <% if (user!=null){%>
           		<h1 style="color: white;"><%=user.getUserName() %></h1>
           	<% }%>
           </div>
           <div class="col-2 d-flex justify-content align-items-center">
              <button class="btn btn-secondary" onclick="on1()">SignUp</button>
            </div>
          <div class="col-2 d-flex justify-content align-items-center">
          <% if(user!=null){ %>
   <input type="submit" id="hii" formaction="logoutU.htm" value="Logout"/>
             	<% } else{%>
             	 <button class="btn btn-secondary"  onclick="on()"> Login </button>
             	 <% } %>
             	 
          </div>
        </div>

      </div>
    </div>

    <!-- CAROUSEL -->
    <div class="row">
      <div class="col-12" style="background-image: url('11.jpg');
          background-position:center;
          background-repeat:no-repeat;
          background-size:cover;
          height:350px;
          width:200px;"
          >


        <div class="row">
            <div class="col-12" style="margin-top: 45px;display: flex; align-items: center; justify-content: center">
                    <h1>Your Service Expert</h1>
            </div>
        </div>
  

        <!--<select class="ml-5 mt-5" >
                
           <option value="Electrician"></a>Electrician</a></option>
           <option value="Plumber"><a href="https://www.w3schools.com">Plumber</a></option>
           <option value="Cleaner"><a href="https://www.w3schools.com">Cleaner</a></option>
         

               
              </select> -->


              <div class="row">
                <div class="col-12 for" style="display: flex; align-items: center; justify-content: center">
                <select class="btn btn-secondary mt-3" id="mySelect" onchange="myFunction()" style="width: 17rem; height: 3rem;">
                  <option value= "nothing"> --select services--</option>
                  <option    value="electrician" >Electrician</option>
                  <option value="plumber">Plumber</option>
                  <option value="cleaner">Cleaner</option>
                </select>
                </div>
              </div>


      </div>

    </div>


    <!--  ICONS -->
    <div class="row">
        <div class="col-12">
            <section></section>
        </div>
    </div>
  </div>

  <!--Trying.. login.-->
  <div class=" " id="overlay" style="display: none;" >
      <div class="container-fluid">
          <div class="row" >
              <div class="col-3"  onclick="off()"></div>
              <div class="col-6" style="margin: 6px 6px 6px 6px ;" >
              <spr:form action="loginU.htm"  commandName="user" class="bg-light" style="border: black solid">
                  <div style="margin-left: 230px">
                     
           	  <h1 style="color: black;">Login</h1>
           		
                  </div>
                  
                  <div class="form-group row ml-1 ">
                      <label for="inputName" class="col-sm-2 col-form-label">Username</label>
                      <div class="col-sm-10">
                      <spr:input type="text" path="userName" class="form-control" id="inputPassword" placeholder="Full Name"/>
                      </div>
                  </div>
                  <div class="form-group row ml-1 ">
                      <label for="inputPassword" class="col-sm-2 col-form-label">Phone no</label>
                      <div class="col-sm-10">
                      <spr:input type="text" path="phoneNo" class="form-control" id="inputPassword" placeholder="Enter your 10 digit number"/>
                      </div>
                  </div>
                  <div class="form-group row ml-1 ">
                          <label for="inputPassword" class="col-sm-2 col-form-label">Password</label>
                          <div class="col-sm-10">
                          <spr:input type="password" path="userPass" class="form-control" id="inputPassword" placeholder="enter your password"/>
                          </div>
                      </div>
                    
  
                              <div>
                                  <input type="submit"  class="btn btn-secondary" style="margin-left: 130px;margin-bottom: 10px;width: 150px" value="login as Customer" />
                                  <input type="submit"  formaction="loginV.htm" class="btn btn-secondary" style="margin-left: 130px;margin-bottom: 10px;width: 150px" value="login as Vendor"/>
                              </div>
              </spr:form>
              </div>
              <div class="col-3"  onclick="off()"></div>
          </div>
      </div>
  
  </div>

  <!-- Trying signup form-->
  <div class="" id="overlay1"  style="display: none;" >
      <div class="container-fluid" >
          <div class="row">
          <div class="col-3" onclick="off1()"></div>
          <div class="col-6 mt-3" style="border: black 5px solid;background: rgb(186, 190, 192)" >
          <spr:form id="user" action="reg.htm" commandName="user">
          <div>
          <h1 style="margin-left: 230px ">SignUp Form</h1>
          </div>
          <div class="form-group row">
          <label for="inputName" class="col-sm-2 col-form-label">name</label>
          <div class="col-sm-10">
          <spr:input id="inputPassword"  path="userName" name="userName" placeholder="Enter your full Name" type="text" class="form-control" value=""/>
          </div>
          </div>
          <div class="form-group row">
          <label for="inputName" class="col-sm-2 col-form-label">age</label>
          <div class="col-sm-10">
          <spr:input id="inputPassword"  path="age" name="age" placeholder="Enter your age" type="text" class="form-control" value="0"/>
          </div>
          </div>
          <div class="form-group row">
          <label for="inputPassword" class="col-sm-2 col-form-label">Phone no</label>
          <div class="col-sm-10">
          <spr:input id="inputPassword"  path="phoneNo" name="phoneNo" placeholder="Enter your 10 digit number" type="text" class="form-control" value=""/>
          </div>
          </div>
          <div class="form-group row">
          <label for="staticEmail" class="col-sm-2 col-form-label">Email</label>
          <div class="col-sm-10">
          <spr:input id="staticEmail" path="emailId" name="emailId" placeholder="Enter your email id" type="text" class="form-control" value=""/>
          </div>
          </div>
          <div class="form-group row">
          <label for="inputPassword" class="col-sm-2 col-form-label">Password</label>
          <div class="col-sm-10">
          <spr:input id="inputPassword"  path="userPass" name="userPass" placeholder="enter your password" type="password" class="form-control" value=""/>
          </div>
          </div>
          <div class="form-group row">
          <label for="inputPassword" class="col-sm-2 col-form-label">Conform Password</label>
          <div class="col-sm-10">
          <spr:input id="inputPassword" path="userConformPass" name="userConformPass" placeholder="enter your password" type="password" class="form-control" value=""/>
          </div>
          </div>
          <div class="form-group row">
          <label for="inputPassword" class="col-sm-2 col-form-label">Profession</label>
          <div class="col-sm-10">
          <spr:select id="userRole" path="userRole" name="userRole" style="width: 150px">
          <spr:option value="Vendor">Vendor</spr:option>
          <spr:option value="User">User</spr:option>
          </spr:select>
          </div>
          </div>
          <div class="form-group row">
          <label for="inputPassword" class="col-sm-2 col-form-label">About</label>
          <div class="col-sm-10">
          <spr:textarea  path="about"  class="form-control" rows="2" placeholder="enter about"></spr:textarea>
          </div>
          </div>
          <div class="form-group row">
          <label for="inputPassword" class="col-sm-2 col-form-label">Address</label>
          <div class="col-sm-10">
         <spr:textarea  path="address" class="form-control" rows="2" placeholder="enter address"></spr:textarea>
          </div>
          </div>
          <input type="submit" value="submit" class="btn btn-success" style="margin-left: 170px"/>
          </spr:form>
          </div>
          <div class="col-3"onclick="off1()"></div>
          </div>
          </div>
          
  </div>

  <!--Trying 3 Electrician-->
  <div class="" id="overlay2"  style="display: none;" >
      <div class="row">
        <div class="col-3" onclick="off2()"  >
           
            
        </div>
        <div class="col-6">
          <div class=" row bg-secondary fixed-top sticky-top" style="height: 70px">
            
           
            
          
          <div class=" col-12  d-flex justify-content-center align-items-center "> 
             <h1> Electrician</h1>
           
       
        </div>
        </div>
        <div class="row ">
            <!-- <div class="col-3 " >
                <div class="btn btn-dark " style="position:sticky;margin-left:100px; top:500px; cursor: pointer">
                  BACK
              </div>
            </div> -->
        <div class="col-12">
          <div class="card mt-2" style="width: 40rem;">
         
            <div class="card-body">
              <h5 class="card-title">Card title</h5>
              
              <div class=" d-flex  " >
              <p class="card-text ">Some </p>
            </div>
            <div class="d-flex  justify-content-end">
              <input type="checkbox" style="width: 30px;height: 30px" aria-label="Checkbox for following text input">
            </div>
         
          
            </div>
          </div>
          <div class="card mt-2" style="width: 40rem;">
         
            <div class="card-body">
              <h5 class="card-title">Card title</h5>
              
              <div class=" d-flex  " >
              <p class="card-text ">Some </p>
            </div>
            <div class="d-flex  justify-content-end">
              <input type="checkbox" style="width: 30px;height: 30px" aria-label="Checkbox for following text input">
            </div>
         
          
            </div>
          </div>
          <div class="card mt-2" style="width: 40rem;">
         
            <div class="card-body">
              <h5 class="card-title">Card title</h5>
              
              <div class=" d-flex  " >
              <p class="card-text ">Some </p>
            </div>
            <div class="d-flex  justify-content-end">
              <input type="checkbox" style="width: 30px;height: 30px" aria-label="Checkbox for following text input">
            </div>
         
          
            </div>
          </div>
          <div class="card mt-2" style="width: 40rem;">
         
            <div class="card-body">
              <h5 class="card-title">Card title</h5>
              
              <div class=" d-flex  " >
              <p class="card-text ">Some </p>
            </div>
            <div class="d-flex  justify-content-end">
              <input type="checkbox" style="width: 30px;height: 30px" aria-label="Checkbox for following text input">
            </div>
         
          
            </div>
          </div>
          <div class="card mt-2" style="width: 40rem;">
         
            <div class="card-body">
              <h5 class="card-title">Card title</h5>
              
              <div class=" d-flex  " >
              <p class="card-text ">Some </p>
            </div>
            <div class="d-flex  justify-content-end">
              <input type="checkbox" style="width: 30px;height: 30px" aria-label="Checkbox for following text input">
            </div>
         
          
            </div>
          </div>
          <div class="card mt-2" style="width: 40rem;">
         
            <div class="card-body">
              <h5 class="card-title">Card title</h5>
              
              <div class=" d-flex  " >
              <p class="card-text ">Some </p>
            </div>
            <div class="d-flex  justify-content-end">
              <input type="checkbox" style="width: 30px;height: 30px" aria-label="Checkbox for following text input">
            </div>
         
          
            </div>
          </div>
          <div class="card mt-2" style="width: 40rem;">
         
            <div class="card-body">
              <h5 class="card-title">Card title</h5>
              
              <div class=" d-flex  " >
              <p class="card-text ">Some </p>
            </div>
            <div class="d-flex  justify-content-end">
              <input type="checkbox" style="width: 30px;height: 30px" aria-label="Checkbox for following text input">
            </div>
           
         
          
            </div>
          </div>
          <div class="card mt-2" style="width: 40rem; visibility: hidden;" >
         
              <div class="card-body">
                <h5 class="card-title">Card title</h5>
                
                <div class=" d-flex  " >
                <p class="card-text ">Some </p>
              </div>
              <div class="d-flex  justify-content-end">
                <input type="checkbox" style="width: 30px;height: 30px" aria-label="Checkbox for following text input">
              </div>
             
           
            
              </div>
            </div>
          <div class=" row bg-secondary fixed-bottom sticky-bottom" >
            
           
            
          
              <div class="  d-flex justify-content-center col-12  mb-5 mt-4 align-items-center "> 
                  <div class="btn btn-dark  " style="position:sticky; cursor: pointer">
                      <input type="date" name="selectdate">
                      <input type="time" name="selecttime">
                      <a href="#" style="font-weight: bold">Submit</a>
                  </div>
               
           
            </div>
            </div>

          

         

      </div>
      <!-- <div class="col-3 " >
        <div class="btn btn-dark  " style="position:sticky; margin-left:100px; top:500px; cursor: pointer">
          NEXT
      </div>
    </div> -->
      </div>
  
  
    </div>
    <div class="col-3" onclick="off2()" >
       
        
      </div>
</div>
</div>
  <!--Trying 4 Plumber-->
<div class="" id="overlay3"  style="display: none;" >
    <div class="row">
      <div class="col-3"  onclick="off3()">
          <div class="btn btn-dark " style="position:sticky;margin-left:100px; top:500px; cursor: pointer">
              BACK
          </div>
      </div>
      <div class="col-6">
        <div class=" row bg-secondary fixed-top sticky-top" style="height: 70px">
          
         
          
        
        <div class=" col-12  d-flex justify-content-center align-items-center "> 
           <h1> Plumber</h1>
         
     
      </div>
      </div>
      <div class="row ">
          <!-- <div class="col-3 " >
              <div class="btn btn-dark " style="position:sticky;margin-left:100px; top:500px; cursor: pointer">
                BACK
            </div>
          </div> -->
      <div class="col-12">
        <div class="card mt-2" style="width: 40rem;">
       
          <div class="card-body">
            <h5 class="card-title">Card title</h5>
            
            <div class=" d-flex  " >
            <p class="card-text ">Some </p>
          </div>
          <div class="d-flex  justify-content-end">
            <input type="checkbox" style="width: 30px;height: 30px" aria-label="Checkbox for following text input">
          </div>
       
        
          </div>
        </div>
        <div class="card mt-2" style="width: 40rem;">
       
          <div class="card-body">
            <h5 class="card-title">Card title</h5>
            
            <div class=" d-flex  " >
            <p class="card-text ">Some </p>
          </div>
          <div class="d-flex  justify-content-end">
            <input type="checkbox" style="width: 30px;height: 30px" aria-label="Checkbox for following text input">
          </div>
       
        
          </div>
        </div>
        <div class="card mt-2" style="width: 40rem;">
       
          <div class="card-body">
            <h5 class="card-title">Card title</h5>
            
            <div class=" d-flex  " >
            <p class="card-text ">Some </p>
          </div>
          <div class="d-flex  justify-content-end">
            <input type="checkbox" style="width: 30px;height: 30px" aria-label="Checkbox for following text input">
          </div>
       
        
          </div>
        </div>
        <div class="card mt-2" style="width: 40rem;">
       
          <div class="card-body">
            <h5 class="card-title">Card title</h5>
            
            <div class=" d-flex  " >
            <p class="card-text ">Some </p>
          </div>
          <div class="d-flex  justify-content-end">
            <input type="checkbox" style="width: 30px;height: 30px" aria-label="Checkbox for following text input">
          </div>
       
        
          </div>
        </div>
        <div class="card mt-2" style="width: 40rem;">
       
          <div class="card-body">
            <h5 class="card-title">Card title</h5>
            
            <div class=" d-flex  " >
            <p class="card-text ">Some </p>
          </div>
          <div class="d-flex  justify-content-end">
            <input type="checkbox" style="width: 30px;height: 30px" aria-label="Checkbox for following text input">
          </div>
       
        
          </div>
        </div>
        <div class="card mt-2" style="width: 40rem;">
       
          <div class="card-body">
            <h5 class="card-title">Card title</h5>
            
            <div class=" d-flex  " >
            <p class="card-text ">Some </p>
          </div>
          <div class="d-flex  justify-content-end">
            <input type="checkbox" style="width: 30px;height: 30px" aria-label="Checkbox for following text input">
          </div>
       
        
          </div>
        </div>
        <div class="card mt-2" style="width: 40rem;">
       
          <div class="card-body">
            <h5 class="card-title">Card title</h5>
            
            <div class=" d-flex  " >
            <p class="card-text ">Some </p>
          </div>
          <div class="d-flex  justify-content-end">
            <input type="checkbox" style="width: 30px;height: 30px" aria-label="Checkbox for following text input">
          </div>
       
        
          </div>
        </div>
    </div>
    <!-- <div class="col-3 " >
      <div class="btn btn-dark  " style="position:sticky; margin-left:100px; top:500px; cursor: pointer">
        NEXT
    </div>
  </div> -->
    </div>


  </div>
  <div class="col-3"  onclick="off3()">
      <div class="btn btn-dark  " style="position:sticky; margin-left:100px; top:500px; cursor: pointer">
          <a href="www.google.com">NEXT</a>
      </div>
    </div>
</div>
</div>
<!--Trying 4 Cleaner-->
<div class="" id="overlay4"  style="display: none;" >
    <div class="row">
      <div class="col-3"  onclick="off4()">
          <div class="btn btn-dark " style="position:sticky;margin-left:100px; top:500px; cursor: pointer">
              BACK
          </div>
      </div>
      <div class="col-6">
        <div class=" row bg-secondary fixed-top sticky-top" style="height: 70px">
          
         
          
        
        <div class=" col-12  d-flex justify-content-center align-items-center "> 
           <h1> Plumber</h1>
         
     
      </div>
      </div>
      <div class="row ">
          <!-- <div class="col-3 " >
              <div class="btn btn-dark " style="position:sticky;margin-left:100px; top:500px; cursor: pointer">
                BACK
            </div>
          </div> -->
      <div class="col-12">
        <div class="card mt-2" style="width: 40rem;">
       
          <div class="card-body">
            <h5 class="card-title">Card title</h5>
            
            <div class=" d-flex  " >
            <p class="card-text ">Some </p>
          </div>
          <div class="d-flex  justify-content-end">
            <input type="checkbox" style="width: 30px;height: 30px" aria-label="Checkbox for following text input">
          </div>
       
        
          </div>
        </div>
        <div class="card mt-2" style="width: 40rem;">
       
          <div class="card-body">
            <h5 class="card-title">Card title</h5>
            
            <div class=" d-flex  " >
            <p class="card-text ">Some </p>
          </div>
          <div class="d-flex  justify-content-end">
            <input type="checkbox" style="width: 30px;height: 30px" aria-label="Checkbox for following text input">
          </div>
       
        
          </div>
        </div>
        <div class="card mt-2" style="width: 40rem;">
       
          <div class="card-body">
            <h5 class="card-title">Card title</h5>
            
            <div class=" d-flex  " >
            <p class="card-text ">Some </p>
          </div>
          <div class="d-flex  justify-content-end">
            <input type="checkbox" style="width: 30px;height: 30px" aria-label="Checkbox for following text input">
          </div>
       
        
          </div>
        </div>
        <div class="card mt-2" style="width: 40rem;">
       
          <div class="card-body">
            <h5 class="card-title">Card title</h5>
            
            <div class=" d-flex  " >
            <p class="card-text ">Some </p>
          </div>
          <div class="d-flex  justify-content-end">
            <input type="checkbox" style="width: 30px;height: 30px" aria-label="Checkbox for following text input">
          </div>
       
        
          </div>
        </div>
        <div class="card mt-2" style="width: 40rem;">
       
          <div class="card-body">
            <h5 class="card-title">Card title</h5>
            
            <div class=" d-flex  " >
            <p class="card-text ">Some </p>
          </div>
          <div class="d-flex  justify-content-end">
            <input type="checkbox" style="width: 30px;height: 30px" aria-label="Checkbox for following text input">
          </div>
       
        
          </div>
        </div>
        <div class="card mt-2" style="width: 40rem;">
       
          <div class="card-body">
            <h5 class="card-title">Card title</h5>
            
            <div class=" d-flex  " >
            <p class="card-text ">Some </p>
          </div>
          <div class="d-flex  justify-content-end">
            <input type="checkbox" style="width: 30px;height: 30px" aria-label="Checkbox for following text input">
          </div>
       
        
          </div>
        </div>
        <div class="card mt-2" style="width: 40rem;">
       
          <div class="card-body">
            <h5 class="card-title">Card title</h5>
            
            <div class=" d-flex  " >
            <p class="card-text ">Some </p>
          </div>
          <div class="d-flex  justify-content-end">
            <input type="checkbox" style="width: 30px;height: 30px" aria-label="Checkbox for following text input">
          </div>
       
        
          </div>
        </div>
    </div>
    <!-- <div class="col-3 " >
      <div class="btn btn-dark  " style="position:sticky; margin-left:100px; top:500px; cursor: pointer">
        NEXT
    </div>
  </div> -->
    </div>


  </div>
  <div class="col-3"  onclick="off4()">
      <div class="btn btn-dark  " style="position:sticky; margin-left:100px; top:500px; cursor: pointer">
          <a href="www.google.com">NEXT</a>
      </div>
    </div>
</div>
</div>

  <script>
    
  function myFunction() {
   var x = document.getElementById("mySelect").value;
                if(x=="electrician"){
                     // console.log(" hii") ; 
                  on2();}
                 if(x=="plumber"){on3();}
               if(x=="cleaner"){on4();}
    }

      function on() {
        document.getElementById("overlay").style.display = "block";
        document.getElementById("overlay1").style.display = "none";
        document.getElementById("overlay2").style.display = "none";
        document.getElementById("overlay3").style.display = "none";
        document.getElementById("overlay4").style.display = "none";
      }
      
      function off() {
        document.getElementById("overlay").style.display = "none";
      }

      function on1() {
        document.getElementById("overlay1").style.display = "block";
        document.getElementById("overlay").style.display = "none";
        document.getElementById("overlay2").style.display = "none";
        document.getElementById("overlay3").style.display = "none";
        document.getElementById("overlay4").style.display = "none";
      }
      
      function off1() {
        document.getElementById("overlay1").style.display = "none";
      }

      function on2() {
        document.getElementById("overlay2").style.display = "block";
        document.getElementById("overlay1").style.display = "none";
        document.getElementById("overlay").style.display = "none";
        document.getElementById("overlay3").style.display = "none";
        document.getElementById("overlay4").style.display = "none";
      }
    
      
      function off2() {
        document.getElementById("overlay2").style.display = "none";
        document.getElementById("mySelect").value="nothing";
      }
      function on3() {
        document.getElementById("overlay3").style.display = "block";
        document.getElementById("overlay1").style.display = "none";
        document.getElementById("overlay").style.display = "none";
        document.getElementById("overlay2").style.display = "none";
        document.getElementById("overlay4").style.display = "none";

      }
      function off3() {
        document.getElementById("overlay3").style.display = "none";
        document.getElementById("mySelect").value="nothing";
      }
    
      function on4() {
        document.getElementById("overlay4").style.display = "block";
        document.getElementById("overlay1").style.display = "none";
        document.getElementById("overlay").style.display = "none";
        document.getElementById("overlay2").style.display = "none";
        document.getElementById("overlay3").style.display = "none";

      }
      function off4() {
        document.getElementById("overlay4").style.display = "none";
        document.getElementById("mySelect").value="nothing";
      }
      
      </script>
	
</body>
</html>
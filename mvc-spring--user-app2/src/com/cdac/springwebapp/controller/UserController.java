package com.cdac.springwebapp.controller;

import java.io.Console;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.tomcat.util.net.openssl.ciphers.Authentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.cdac.springwebapp.dto.User;
import com.cdac.springwebapp.serv.UserService;



@Controller
public class UserController {
	
	@Autowired
	private UserService userService;
	
	@RequestMapping(value = "/prep_login_form.htm")
	public String prepLoginForm(ModelMap model) {
		model.put("user", new User());
		return "index";
	}
	
	@RequestMapping(value = "/prep_reg_form.htm")
	public String prepRegForm(ModelMap model) {
		model.put("user", new User());
		return "reg_form";
	}
	
	@RequestMapping(value = "/reg.htm")
	public String register(User user,ModelMap model) {
		System.out.println("inside reg");
		//user.setUserPass(Encryption.encrypt(user.getUserPass()));
		userService.createUser(user);
		
		model.put("user", new User());
		return "index";
	}
	
	@RequestMapping(value = "/loginU.htm")
	public String login(User user,ModelMap model,HttpSession session) {
		//user.setUserPass(Encryption.encrypt(user.getUserPass()));
		boolean b = userService.checkUser(user);
		if(b) {
			User ulist1 = userService.selectUser(user);
			session.setAttribute("user", ulist1);
			//model.put("user",ulist1);
			//User ulist1 = userService.selectUser(user);
			return "index";
		}
		//model.put("user", new User());
		return "userloginError";
	}
	
	@RequestMapping(value = "/logoutU.htm")
	public String logout(@RequestParam User obj,HttpSession session) {
		
		//user.setUserPass(Encryption.encrypt(user.getUserPass()));
	//User ulist1 = userService.selectUser(object);
			 obj.setActive(0);
	session.removeAttribute("user");
		
	
			
			return "userloginError";
		
		
	}
	
	
	@RequestMapping(value = "/loginV.htm")
	public String loginV(User user,ModelMap model,HttpSession session) {
		
		boolean b = userService.checkVendor(user);
		if(b) {
			User vendor =(User)userService.getVendorserv(user);
			session.setAttribute("vendork", vendor);
			return "vendor_home";
		}
		//model.put("user", new User());
		return "userloginError";
	}
	
	@RequestMapping(value = "/user_list.htm")
	public String userList(User user,ModelMap model) {
		List<User> ulist = userService.selectAllUsers();
		model.put("ul", ulist);
		return "user_list";
	}
	
	@RequestMapping(value = "/vendor_list.htm")
	public String vendorList(User user,ModelMap model) {
		List<User> ulist1 = userService.selectAllVendor(user);
		model.put("ul1", ulist1);
		return "vendor_list";
	}
	
	@RequestMapping(value = "/delete_user.htm")
	public String deleteUser(@RequestParam int userId,ModelMap model) {
		userService.removeUser(userId);
		List<User> ulist = userService.selectAllUsers();
		model.put("ul", ulist);
		return "user_list";
	}
	
}